#include <thread>
#include <mutex>
#include <condition_variable>
#include <iostream>
#include <vector>
#include "thread_safe_queue.h"

std::condition_variable cv;
thread_safe_queue<int> Queue(5);
std::size_t tasks = 10;
std::mutex mtx1;
int main(){

    std::thread producer([](){
        std::this_thread::sleep_for(std::chrono::milliseconds(50));
        for (int i = 0; i < 10; i++){
            Queue.enqueue(i);
            std::lock_guard<std::mutex> lock(mtx1);
            std::cout << "push " << i << std::endl;

        }
    });
    producer.detach();
    std::vector<std::thread> consumers;
    for (int i = 0; i < 10; i++){
        consumers.emplace_back(std::thread ([](){
            std::this_thread::sleep_for(std::chrono::milliseconds(50));
            int val;

            Queue.pop(val);
            std::lock_guard<std::mutex> lock(mtx1);
            std::cout << "pop " << val << " ";
            if (val % 2 == 0)
                std::cout << "even" << std::endl;
            else
                std::cout << "odd" << std::endl;
            tasks--;
        }));
        consumers[i].detach();
    }


    while (tasks != 0){
        //wait
    }
    Queue.shutdown();

    return 0;
}


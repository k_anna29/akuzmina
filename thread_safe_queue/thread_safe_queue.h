#ifndef THREAD_SAFE_QUEUE
#define THREAD_SAFE_QUEUE

#include <queue>
#include <mutex>
#include <condition_variable>

template <class T>
class thread_safe_queue{
public:
    thread_safe_queue(std::size_t capacity);

    void enqueue(T item);
    void pop(T& item);
    void shutdown();

private:
    std::queue<T> queue;
    std::size_t capacity;
    std::mutex mtx;
    bool accessibility;

    std::condition_variable queue_not_empty;
    std::condition_variable queue_not_full;
    std::condition_variable queue_is_empty;

};

template<class T> inline
thread_safe_queue<T>::thread_safe_queue(std::size_t _capacity){
    capacity = _capacity;
    accessibility = true;
}

template <class T> inline
void thread_safe_queue<T>::pop(T& item){

    std::unique_lock<std::mutex> lock(mtx);

    if (accessibility){

        //ждет, пока нечего извлекать
        if (queue.empty()){
            queue_not_empty.wait(lock, [this] () {return !queue.empty();});
        }

        item = queue.front();
        queue.pop();

        //в очереди появилось место
        if (queue.size() == capacity - 1){
            queue_not_full.notify_one();
        }

        //очередь пуста, если вызван shutdown можно завершиться
        if (queue.empty()){
            queue_is_empty.notify_one();
        }
    }

}

template <class T> inline
void thread_safe_queue<T>::enqueue(T item){

    std::unique_lock<std::mutex> lock(mtx);
    if (accessibility){
        if (queue.size() == capacity){
            queue_not_full.wait(lock, [this](){
                return queue.size() < capacity;
            });
        }

        queue.push(item);

        queue_not_empty.notify_one();
    }
}

template <class T> inline
void thread_safe_queue<T>::shutdown(){
    std::unique_lock<std::mutex> lock(mtx);
    accessibility = false;
    queue_is_empty.wait(lock, [this](){ return queue.empty();});
    std::cout << "shutdown" << std::endl;

}

#endif // THREAD_SAFE_QUEUE


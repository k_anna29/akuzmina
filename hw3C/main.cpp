#include <stdlib.h>
#include <iostream>
#include <string>
#include <vector>
int _sizeOfAlphabet = 256;
char _minChar = '\0';

using namespace std;
//input is cycled string

class SegmentTree{
public:
    SegmentTree(int n, vector<int> _array){
        tree.resize(4*n);//можно уменьшить.
        array = _array;
        num = n;
    }
    void BuildTree();
    int GetMin(int i, int j);

private:
    void build(int i, int j, int par);
    int getMin(int i, int j, int L, int R, int v);
    vector<int> tree;
    vector<int> array;
    int num;
};

void SegmentTree::BuildTree(){
    build(0, num - 1, 0);
}

void SegmentTree::build(int a, int b, int par){
    if (a == b){
        tree[par] = array[a];
    }
    else{

        build(a, (int)(a + b)/2, 2 * par + 1);
        build((int)(a + b)/2 + 1, b, 2 * par + 2);
        //min
        tree[par] = min (tree[par * 2 + 2], tree[par * 2 + 1]);

    }
}

int SegmentTree::GetMin(int i, int j){
    return getMin(i, j, 0, num - 1, 0);
}

int SegmentTree::getMin(int i, int j, int L, int R, int v){
    if (i == L && j == R){
        return tree[v];
    }

    else if (j <= (L + R)/2) //всё слева
            return getMin(i, j, L, (int)(L + R) /2, 2 * v + 1);
        else if (i > (L + R)/2) // все справа
                return getMin(i, j, (int)((L + R)/2) + 1, R, 2 * v + 2);
             else{
        int q = getMin(i, (L + R)/2, L, (L + R)/2, 2 * v + 1);
        int p = getMin((L + R)/2, j, (L + R)/2, R, 2 * v + 2);
        return min(p, q);


    }
}



vector<int> BuildSuffixArray(string str, int strLen){
    vector<int> SuffixArray(strLen, 0);
    vector<int> ClassesOfEquality(strLen, 0);
    int numOfClasses = 0;

    //first step, counting sort
    vector<int> countingSortAray(_sizeOfAlphabet, 0);
    for (int i = 0; i < strLen; i++){
        countingSortAray[str[i]]++;
    }

    //array will be array of posotions
    for (int i = 1; i < _sizeOfAlphabet; ++i){
        countingSortAray[i] += countingSortAray[i - 1];
    }
    //int countingSortArray s[i] in pos csa[s[i]] - 1
    //make suffixarray
    for (int i  = 0; i < strLen; i++){
        //--countingSortAray[str[i]];//array from 1 not from 0
        SuffixArray[--countingSortAray[str[i]]] = i;
    }
    //make classes
    numOfClasses = 1;
    for (int i = 1; i < strLen; i++){
        //if previous suffix in SA less than current
        if (str[SuffixArray[i]] != str[SuffixArray[i - 1]])
            numOfClasses++;
        ClassesOfEquality[SuffixArray[i]] = numOfClasses - 1;
    }
    //correct


    //next steps by prev dividing into 2 parts;
    //to r, that 2^r=k <= n < 2^(r+1)
    //counting sort by second elements;
    vector<int> SuffixArray2(strLen, 0);
    vector<int> ClassesOfEquality2(strLen, 0);
    //numberf if iterations until k greater

    for (int k = 1; k < strLen; k = (k << 1)){
        //moving suffixarray (second parts)
        vector<int> countingSortAray(strLen, 0);
        vector<int> SuffixArray2(strLen, 0);
        for (int i = 0; i < strLen; i++){
            SuffixArray2[i] = SuffixArray[i] - k;
            //mod n, because cyclic
            if (SuffixArray2[i] < 0)
                SuffixArray2[i] += strLen;
        }
        //теперь упорядочены по вторым

        //counting classes
       for(int i = 0; i < strLen; i++)
            countingSortAray[ClassesOfEquality[SuffixArray2[i]]]++;

       for (int i = 1; i < numOfClasses; i++)
           countingSortAray[i] += countingSortAray[i - 1];

       for (int i = strLen - 1; i >= 0; --i)
           SuffixArray[--countingSortAray[ClassesOfEquality[SuffixArray2[i]]]] = SuffixArray2[i];
       ClassesOfEquality2[SuffixArray[0]] = 0;
       numOfClasses = 1;
       for (int i = 1; i < strLen; i++){
           int secondPartCurr = (SuffixArray[i] + k) % strLen;
           int secondPartPrev = ((SuffixArray[i - 1] + k)) % strLen;
           if (ClassesOfEquality[SuffixArray[i]] != ClassesOfEquality[SuffixArray[i - 1]] || ClassesOfEquality[secondPartCurr] != ClassesOfEquality2[secondPartPrev])
               numOfClasses++;
           ClassesOfEquality2[SuffixArray[i]] =  numOfClasses - 1;
       }
       ClassesOfEquality = ClassesOfEquality2;

    }
    //correct
    return SuffixArray;
}

vector<int> BuildLCP(string str, vector<int> SuffixArray, int strLen){
    //vector<int> SuffixArray = BuildSuffixArray(str);
    //LCP between sa[i] and sa[i-1]
    vector<int> LCP(strLen, 0);
    vector<int> SuffixPositions(strLen);
    for (int i = 0; i < strLen; i++){
        SuffixPositions[SuffixArray[i]] = i;
    }
    int lcp = 0;
    for (int i = 0; i < strLen; i++){
        //next is witout 1 letter, sonit's lcp eqor greater lcp - 1
        if (lcp > 0)
            lcp--;
        if (SuffixPositions[i] == strLen - 1){
            LCP[strLen - 1] = -1;
            lcp = 0;
        }
        else{
            int j = SuffixArray[SuffixPositions[i] + 1];//next
            for ( ; max(i + lcp, j + lcp) < strLen && str[i + lcp] == str[j + lcp]; lcp++);
            LCP[SuffixPositions[i]] = lcp;
        }
    }
    return LCP;

}

string FindRepetedSubstring(string str, int K){
    int strLen = str.size();
    str.push_back(_minChar);
    //str.size
    str.insert(str.end(), str.begin(), --str.end());//making cycle string str!str
    vector<int> SuffixArray = BuildSuffixArray(str, strLen);
    vector<int> LCPArray = BuildLCP(str, SuffixArray, strLen);
    //find part where LCP >= 1 and size >= k
    SegmentTree T(strLen - 1, LCPArray);
    T.BuildTree();
    int Min = 0;
    int currMin = 0;
    int minPos;//номер суффикса
    for(int i = 0; i + K - 1 < strLen; i++){
        currMin = T.GetMin(i, i + K - 2);
        if (currMin > Min){
            Min = currMin;
            minPos = i;
        }
    }
    return str.substr(SuffixArray[minPos], Min);


}

int main(){
    string str;

    cin >> str;
    int K;
    cin >> K;
    if (K == 1)
        cout << str;
    else
        cout << FindRepetedSubstring(str, K);

    return 0;
}
